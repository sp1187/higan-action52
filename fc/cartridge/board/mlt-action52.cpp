
struct MLT_Action52 : Board {

  uint2 prg_chip;
  uint5 prg_bank;
  bool prg_mode;

  uint6 chr_bank;

  bool mirror;

  uint8 prg_read(unsigned addr) {
    if(addr & 0x8000){
      uint19 chipaddr;

      if(prg_mode == 0)
        chipaddr = ((prg_bank & ~1) << 14) | (addr & 0x7fff);
      else
        chipaddr = (prg_bank << 14) | (addr & 0x3fff);

      switch(prg_chip){
        case 0:
          return prgrom.read(chipaddr);
        case 1:
          return prgrom.read(0x080000 | chipaddr);
        case 3:
          return prgrom.read(0x100000 | chipaddr);
      }
    }
    return cpu.mdr();
  }

  void prg_write(unsigned addr, uint8 data) {
    if(addr & 0x8000) {
      mirror = (addr & 0x2000) >> 13;
      prg_chip = (addr & 0x1800) >> 11;
      prg_bank = (addr & 0x7c0) >> 6;
      prg_mode = (addr & 0x20) >> 5;
      chr_bank = ((addr & 0x0f) << 2) | (data & 0x03);
    }
  }

  uint8 chr_read(unsigned addr) {
    if(addr & 0x2000) {
      if(mirror) addr = ((addr & 0x0800) >> 1) | (addr & 0x03ff);
      return ppu.ciram_read(addr & 0x07ff);
    }
    addr = (chr_bank * 0x2000) + (addr & 0x1fff);
    return Board::chr_read(addr);
  }

  void chr_write(unsigned addr, uint8 data) {
    if(addr & 0x2000) {
      if(mirror) addr = ((addr & 0x0800) >> 1) | (addr & 0x03ff);
      return ppu.ciram_write(addr & 0x07ff, data);
    }
    addr = (chr_bank * 0x2000) + (addr & 0x1fff);
    Board::chr_write(addr, data);
  }

  void power() {
  }

  void reset() {
    prg_chip = 0;
    prg_bank = 0;
    prg_mode = 0;
    chr_bank = 0;
    mirror = 0;
  }

  void serialize(serializer& s) {
    Board::serialize(s);

    s.integer(prg_chip);
    s.integer(prg_bank);
    s.integer(prg_mode);
    s.integer(chr_bank);
    s.integer(mirror);
  }

  MLT_Action52(Markup::Node& document) : Board(document) {
  }

};
